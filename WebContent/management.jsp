<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
<!--
function dispstop(name){
	if(confirm(name + '停止してよろしいですか')) {
		return true;

	}else{
		return false;
	}

}
function dispstart(name){

	if(confirm(name + '復活してよろしいですか')) {
		return true;

	}else{
		return false;
	}
}


//-->
</script>
<title>ユーザー管理</title>
<link href="./css/management.css" rel="stylesheet" type="text/css">
</head>
<body>


<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="editUsers">
						<li><c:out value="${editUsers}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<div class="main-contents">

			<a class="sign" href="signup">新規登録</a>
			<br/><br/>
			<a class="home" href="./">ホーム画面に戻る</a>

	</div>
	<br/>



	<table border="2" class="managementable">
		<tr>
		<th>名前</th><th>ログインID</th><th>支店</th><th>部署・役職</th><th>編集</th><th>ユーザー状況</th>
		</tr>
		<c:forEach items="${managements}" var="managements">

				<input type="hidden" value="${managements.id }" />
				<tr>
				<td>
				<span class="name"> <c:out value="${managements.name}" /></span></td>
				<td><span class="login_id">
				<c:out value="${managements.login_id}" />
				</span></td>
				<td><span class="branch_name"> <c:out
						value="${managements.branch_name}" />
				</span></td>
				<td><span class="position_name"> <c:out
						value="${managements.position_name}" />
				</span></td>

			<td>
			<form name="UserId" action="edit" >

			<button class="editbutton" type="submit" name="userid" value="${managements.id}">
			編集
			</button>

			</form></td>
			<td>
			<form action="managements" method="post" >
			<c:choose>

			<c:when test="${loginUser.id == managements.id}">
			<c:out value="ログイン中" />
			<input type="hidden" name="userid" value="${managements.id}">
			</c:when>
			<c:otherwise>
			<c:choose>
			<c:when test="${managements.isstoped == 0 }">
			<button class="edit" type="submit" name="stop"  value="${managements.isstoped}" onClick="return dispstop('${managements.name}')">
			停止</button><input type="hidden" name="userid" value="${managements.id}">
			</c:when>
			<c:otherwise>
			<button class="edit" type="submit" name="stop" value="${managements.isstoped}" onClick="return dispstart('${managements.name}')">
			復活</button><input type="hidden" name="userid" value="${managements.id}">
			</c:otherwise>
			</c:choose>
			</c:otherwise>
			</c:choose>

			</form></td></tr>


		</c:forEach>

	</table>
<div class="copyright"> Copyright(c)y.Katagiri</div>

</body>
</html>