
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${EditUser.name}編集</title>
<link href="./css/edit.css" rel="stylesheet" type="text/css">
</head>
<body>

	<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="EditUsers">
						<li><c:out value="${EditUsers}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<div class="message">
	<form  class="messageform" action="edit" method="post">
		<input name="userid" value="${EditUser.id}" id="id" type="hidden" />
		<input name="getpassword" value="${EditUser.password}" type="hidden" />
		<input name="getlogin_id" value="${EditUser.login_id }" type="hidden"/>
		<label for="login_id">ログインID(半角英数字6文字以上20文字以内)</label>
		<input name="login_id" value="${EditUser.login_id}" id="login_id" /><br />
		<label for="password">パスワード(登録用)記号を含む半角英数字6文字以上20文字以内</label>
		<input name="password" id="password" type="password" /><br />
		<label for="password">パスワード(確認用)</label>
		<input name="checkpassword" id="checkpassword" type="password" /><br />
		<label for="name">名前(10文字以内)</label>
		<input name="name" value="${EditUser.name}" id="name" /><br />

		<c:choose>
		<c:when test="${loginUser.id == EditUser.id }">
		<c:forEach items="${branchs}" var="branch">
		<c:if test="${loginUser.branch_id == branch.id}">
		<input type="hidden" value="${EditUser.branch_id }"name="branch_id"/>
		<p>支店　<c:out value="${branch.branch_name}"/></p>
		</c:if>
		</c:forEach>
		<br/>
		<c:forEach items="${positions}" var="position">
		<c:if test="${EditUser.position_id == position.id}">
		<p>部署・役職　<c:out value="${position.position_name}"/></p>
		<input type="hidden" value="${EditUser.branch_id }" name="position_id"/>
		</c:if>
		</c:forEach>
		</c:when>
		<c:otherwise>
		<div class="select-box">
		支店<br/>
		<SELECT	name="branch_id">
			<c:forEach items="${branchs}" var="branch">

				<c:choose>
					<c:when test="${EditUser.branch_id == branch.id}">

						<option value="${branch.id}" selected>${branch.branch_name}</option>
					</c:when>
					<c:otherwise>
						<option value="${branch.id}">${branch.branch_name}</option>
					</c:otherwise>
				</c:choose>


			</c:forEach>
		</SELECT> <br /> 部署・役職<br/><select name="position_id">
			<c:forEach items="${positions}" var="position">
				<c:choose>
					<c:when test="${EditUser.position_id == position.id}">
						<option value="${position.id}" selected>${position.position_name}</option>
					</c:when>
					<c:otherwise>
						<option value="${position.id}">${position.position_name}</option>
					</c:otherwise>
				</c:choose>

			</c:forEach>
		</select></div></c:otherwise></c:choose> <br /> <input class="update" type="submit" value="更新" /> <br /> <a href="management">ユーザー管理画面へ戻る</a>
	</form></div>



 <div class="copyright"> Copyright(c)y.Katagiri</div>




</body>
</html>