<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript">
<!--

function disdelete(){
	if(confirm('削除してよろしいですか？')) {
		return true;

	}else{
		return false;
	}
}

//-->
</script>
<title>掲示板</title>
<link href="./css/top.css" rel="stylesheet" type="text/css">
</head>
<body>

	<c:if test="${ not empty errormessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errormessages}" var="comments">
						<li><c:out value="${comments}" />
					</c:forEach>
				</ul>
				</div>
			<c:remove var="errormessages" scope="session" />
		</c:if>
		<div class="errorMessages">
		<c:if test="${not empty filtermessage}">
		<c:out value="${filtermessage}"/>
		<c:remove var="filtermessage" scope="session" />
		</c:if></div>




	<form action="message">
		<button type="submit" class="newcomment" >新規投稿画面</button>
	</form>
	<c:if test="${loginUser.position_id == 1}" >
	<c:if test="${loginUser.branch_id == 1 }">
	<form action="management">
		<button type="submit" class="management">ユーザー管理画面</button>
	</form>
	</c:if>
	</c:if>
	<br />
	<form action="logout">
	<button type="submit" name="logout" class="logout">ログアウト</button>
	</form>

	<form action="./">
	カテゴリー
	<input type="text" name="category" value="${category }" /><br/>
	日付指定　
	<input type="date" name="start" value="${start}" />
	～
	<input type="date" name="stop" value="${stop}" />
	<input class="search" type="submit" value="検索"/><a class="reset" href="./">リセット</a><br/>
	</form>




	<c:forEach items="${homeMessages}" var="homeMessage" >
		<div class="box"><div class="commentbox">
		件名
		<div class="categorybox">
		<c:out value="${homeMessage.subject}" /></div>
		<br />

		本文<div class="thetext"><pre><c:out value="${homeMessage.text}" /></pre></div>
		<br />
		カテゴリー　<div class="incategory"><c:out value="${homeMessage.category }" /></div>
		<br /><br/>

		投稿者<c:out value="${homeMessage.name }" />
		投稿日時<c:out value="${homeMessage.created_date}" />
		<br />
		<form action="messagedelete" method="post" >
		<c:if test="${loginUser.id == homeMessage.user_id }">
		<input type="hidden" name="message_id" value="${homeMessage.id}"/>
		<input class="delete" type="submit" value="投稿削除" onClick="return disdelete()" />
		</c:if>
		</form></div>

		<c:forEach items="${comments}" var="comments">
		<c:if test="${homeMessage.id == comments.message_id }">
		<div class="commentuser">
		コメント投稿者　<c:out value="${comments.name}" /><br/>
		<div class="precomment"><pre><c:out value="${comments.comment}"/></pre></div>

		コメント投稿日時　<c:out value="${comments.created_date}" /></div><br/>
		<form action="commentdelete" method="post">
		<c:if test="${loginUser.id == comments.user_id }" >
		<input type="hidden" name="comment_id" value="${comments.id }"/>
		<input class="delete" id="delete" type="submit" value="コメント削除" onClick="return disdelete()" />
		</c:if>
		</form>

		</c:if>


		</c:forEach>
		<form action="comment" method="post">
	<input type="hidden" name="message_id" value="${homeMessage.id}"/>
		<textarea class="commentarea" name="comment" cols="50" rows="5" ><c:if test="${ homeMessage.id == getmessage_id }" >${errorcomment}</c:if></textarea>
		<input type="hidden" name="id" value="loginuser.id" />


			<input class="comment" type="submit" value="コメント"/>

		</form></div>

		<c:out value="${comment.user_id }" />
		<form action="commentdelete" method="post" >


		</form>
		<br/>


	</c:forEach>
	<c:remove var="errorcomment" scope="session"/>


	<div class="copyright">Copyright(c)y.Katagiri</div>
</body>
</html>