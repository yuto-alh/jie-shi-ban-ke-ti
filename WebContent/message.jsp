<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/message.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="form-area">

	<c:if test="${ not empty errormessage }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errormessage}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errormessage" scope="session" />
		</c:if>
		<div class="message">
		<form class="messageform" action="message" method="post">
			<label for="subject">件名(30文字以内)</label>
			<input name="subject" value="${subject}">
			<br/>

			<label for="category">カテゴリー(10文字以内)</label>
			<input name="category" value="${category}"><br/>
			本文(1000文字以内)<br/>
			<textarea name="text" cols="50" rows="20">${text}</textarea>
			<input type="hidden" name="id" value="loginuser.id" /><br/>
			<input class="put" type="submit" value="送信" /><br/>
			<a href="./">ホーム画面へ戻る</a>


		</form></div>
	</div>
 <div class="copyright"> Copyright(c)y.Katagiri</div>


</body>
</html>