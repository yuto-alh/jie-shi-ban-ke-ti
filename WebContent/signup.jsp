<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
<link href="./css/signup.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="message">
		<form class="messageform" action="signup" method="post">
			<br /> <label for="login_id">ログインID(半角英数字6文字以上20文字以内)</label>
					<input name="login_id"value="${signupUser.login_id}" id="login_id" /><br />
					<label for="password">パスワード(登録用)記号を含む半角英数字6文字以上20文字以内</label>
				<input name="password" type="password" id="password" /> <br />
				<label for="checkpassword">パスワード(確認用)</label>
				<input name="checkpassword" type="password" id="checkpassword"/><br/>
				<label for="name">名前(10文字以内)</label>
			<input name="name" value="${signupUser.name}" id="name" /> <br />

			支店<br/>
			<div class="select-box">
			<SELECT	name="branch_id">
			<c:forEach items="${branchs}" var="branch">
			<c:choose>
					<c:when test="${signupUser.branch_id == branch.id}">
						<option value="${branch.id}" selected>${branch.branch_name}</option>
					</c:when>
					<c:otherwise>
						<option value="${branch.id}">${branch.branch_name}</option>
					</c:otherwise>
				</c:choose>
				</c:forEach>
				</SELECT></div><c:out value="${branch_id }"/><c:out value="${branch_id }"/>

			部署・役職<br/><div class="select-box">
			<select name="position_id">
			<c:forEach items="${positions}" var="position">
				<c:choose>
					<c:when test="${signupUser.position_id == position.id}">
						<option value="${position.id}" selected>${position.position_name}</option>
					</c:when>
					<c:otherwise>
						<option value="${position.id}">${position.position_name}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			</select></div>
			<br/>
			<input class="sign" type="submit" value="登録" /> <br />
			<a href="management">ユーザー管理画面へ戻る</a>
		</form></div>

 <div class="copyright"> Copyright(c)y.Katagiri</div>


	</div>
</body>
</html>

