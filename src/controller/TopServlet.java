package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.HomeMessage;
import service.CommentService;
import service.HomeMessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String start = request.getParameter("start");
		String stop = request.getParameter("stop");
		String category = request.getParameter("category");

		List<HomeMessage> homeMessages = new HomeMessageService().getHomeMessage(category, start, stop);
		List<Comment> comments = new CommentService().getComment();

		request.setAttribute("homeMessages", homeMessages);
		request.setAttribute("comments", comments);
		request.setAttribute("category", category);
		request.setAttribute("start", start);
		request.setAttribute("stop", stop);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}