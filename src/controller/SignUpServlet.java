package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.LoginIdCheck;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {



		List<Branch> branchs = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();

		request.setAttribute("positions", positions);
		request.setAttribute("branchs", branchs);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setPosition_id(request.getParameter("position_id"));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {

			List<Branch> branchs = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			User signupUser = new User();
			signupUser.setLogin_id(request.getParameter("login_id"));
			signupUser.setName(request.getParameter("name"));
			signupUser.setBranch_id(request.getParameter("branch_id"));
			signupUser.setPosition_id(request.getParameter("position_id"));


			session.setAttribute("errorMessages", messages);
			request.setAttribute("signupUser", signupUser);
			request.setAttribute("positions", positions);
			request.setAttribute("branchs", branchs);
			request.getRequestDispatcher("signup.jsp").forward(request, response);

		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String checkPassword = request.getParameter("checkpassword");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));

		if(StringUtils.isBlank(login_id) == true){
			messages.add("ログインIDを入力してください");
		}else if(!(6 <= login_id.length() && login_id.length() <= 20)){
			messages.add("ログインIDの定義に従っていません");
		}else if(!(login_id.matches("^[\\w]+$"))){
			messages.add("ログインIDの定義に従っていません");
		}
		if(LoginIdCheck.Logincheck(login_id) != true){
			messages.add("このログインIDは使用されています");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if(!(StringUtils.isBlank(password) == true)){
			if(!(StringUtils.isBlank(checkPassword) == true)){
				if(!(6 <= password.length() && password.length() <= 20)){
					messages.add("パスワードの定義に従っていません");
				}else if(password.matches("[ -~]")){
					messages.add("パスワードの定義に従っていません");

				}else if(!(password.equals(checkPassword))){
					messages.add("パスワードが一致しません");
				}
			}else{
				messages.add("パスワードが一致しません");
			}
		}else if(!(StringUtils.isBlank(checkPassword) == true)){
			messages.add("パスワードが一致しません");
		}
		if(StringUtils.isBlank(name) == true){
			messages.add("名前を入力してください");
		}else if(!(1 <= name.length() && name.length() <= 10)){
			messages.add("名前の定義に従っていません");

		}
		if(branch_id != 1){
			if(position_id == 1){
				messages.add("支店で総務人事担当はできません");
			}

		}
		if(position_id == 3){
			if(branch_id == 1){
				messages.add("本社に支店長はできません");
			}
		}



		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}