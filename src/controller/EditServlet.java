package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Edit;
import beans.Position;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.EditCustomService;
import service.EditService;
import service.IdCheckService;
import service.LoginIdCheck;
import service.PositionService;


@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)throws IOException, ServletException{

		List<String> editUsers = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(getisValid(request, editUsers) == true){

			String EditGetUser = request.getParameter("userid");
			int editGetUser = Integer.parseInt(EditGetUser);
			Edit EditUser = new EditService().getEdit(editGetUser);

			List<Branch> branchs = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();

			request.setAttribute("EditUser", EditUser);
			request.setAttribute("branchs", branchs);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("/edit.jsp").forward(request, response);


		}else{
			session.setAttribute("errorMessages", editUsers);
			response.sendRedirect("management");
		}

	}



	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {



		List<String> EditUsers = new ArrayList<String>();
		HttpSession session = request.getSession();
		Edit editUser = getCustomUser(request);


		if(isValid(request, EditUsers) == true){

			try{
				String getPassword =request.getParameter("getpassword");
				String inputPassword = request.getParameter("password");
				new EditCustomService().update(editUser, getPassword, inputPassword);

			}catch (NoRowsUpdatedRuntimeException e){
				request.setAttribute("EditUser", editUser);
				request.getRequestDispatcher("/edit").forward(request, response);
				return;
			}
			session.setAttribute("EditUser", editUser);
			response.sendRedirect("management");
		}else{
			String login_id = request.getParameter("login_id");
			List<Branch> branchs = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("branchs", branchs);
			request.setAttribute("positions", positions);
			request.setAttribute("EditUser", editUser);
			request.setAttribute("login_id", login_id);
			session.setAttribute("errorMessages", EditUsers);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}


	private Edit getCustomUser(HttpServletRequest request)
			throws IOException, ServletException {

		Edit EditUser = new Edit();

		String ID = request.getParameter("userid");
		int Id = Integer.parseInt(ID);
		EditUser.setId(Id);
		EditUser.setLogin_id(request.getParameter("login_id"));
		String getpassword = request.getParameter("getpassword");
		String password = request.getParameter("password");

		if(StringUtils.isBlank(password) == true){

			EditUser.setPassword(getpassword);
		}else{

			EditUser.setPassword(password);
		}

		EditUser.setName(request.getParameter("name"));
		String branch_Id = request.getParameter("branch_id");
		int Branch_Id = Integer.parseInt(branch_Id);
		EditUser.setBranch_id(Branch_Id);
		String position_Id = request.getParameter("position_id");
		int Position_Id = Integer.parseInt(position_Id);
		EditUser.setPosition_id(Position_Id);

		return EditUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> EditUsers){
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String checkPassword =request.getParameter("checkpassword");
		String getlogin_id = request.getParameter("getlogin_id");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));




		if(StringUtils.isBlank(login_id) == true){
			EditUsers.add("ログインIDを入力してください");
		}else if(!(6 <= login_id.length() && login_id.length() <= 20)){
			EditUsers.add("ログインIDの定義に従っていません");
		}else if(!(login_id.matches("^[\\w]+$"))){
			EditUsers.add("ログインIDの定義に従っていません");
		}
		if(!(login_id.equals(getlogin_id))){

			if(LoginIdCheck.Logincheck(login_id) != true){
				EditUsers.add("このログインIDは使用されています");
			}
		}

		if(!(StringUtils.isBlank(password) == true)){
			if(!(StringUtils.isBlank(checkPassword) == true)){
				if(!(6 <= password.length() && password.length() <= 20)){
					EditUsers.add("パスワードが定義に従っていません");
				}else if(password.matches("[ -~]")){
					EditUsers.add("パスワードの定義に従っていません");
				}else if(!(password.equals(checkPassword))){
					EditUsers.add("パスワードが一致しません");
				}
			}else{
				EditUsers.add("パスワードが一致しません");
			}
		}else if(!(StringUtils.isBlank(checkPassword) == true)){
			EditUsers.add("パスワードが一致しません");
		}
		if(StringUtils.isBlank(name) == true){
			EditUsers.add("名前を入力してください");
		}else if(!(1 <= name.length() && name.length() <= 10)){
			EditUsers.add("名前の定義に従っていません");
		}
		if(branch_id != 1){
			if(position_id == 1){
				EditUsers.add("支店で総務人事担当はできません");
			}

		}
		if(position_id == 3){
			if(branch_id == 1){
				EditUsers.add("本社に支店長はできません");
			}
		}


		if(EditUsers.size() == 0){
			return true;

		}else{
			return false;
		}
	}
	private boolean getisValid(HttpServletRequest request, List<String> editUsers){


		String id = request.getParameter("userid");
		if(id != null){
			if(id.matches("^[0-9]{1,4}$")){
				int Id = Integer.parseInt(id);
				if(IdCheckService.IdCheck(Id) == null){
					editUsers.add("不正なパラメーターです");
				}

			}else {
				editUsers.add("不正なパラメーターです");

			}

		}else{
			editUsers.add("不正なパラメーターです");
		}


		if(editUsers.size() == 0){
			return true;
		}else{
			return false;
		}
	}


}
