package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserStopService;

@WebServlet(urlPatterns = {"/managements" })

public class UserStopServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int isstoped = Integer.parseInt(request.getParameter("stop"));

		int userId = Integer.parseInt(request.getParameter("userid"));


		if(isstoped == 0){
			isstoped = 1;
			UserStopService stopEdit = new UserStopService();
			stopEdit.getStop(isstoped, userId);

		}else{
			isstoped = 0;
			UserStopService stopEdit = new UserStopService();
			stopEdit.getStop(isstoped, userId);

		}
		response.sendRedirect("management");
	}


}
