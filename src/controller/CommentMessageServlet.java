package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentMessageService;

@WebServlet(urlPatterns = {"/comment"})

public class CommentMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		if (isValid(request,comments) == true){


			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setUser_id(user.getId());
			comment.setComment(request.getParameter("comment"));
			comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));

			new CommentMessageService().Commentmessage(comment);
			response.sendRedirect("./");

		}else{
			String comment = request.getParameter("comment");
			int getmessage_id = Integer.parseInt(request.getParameter("message_id"));

			session.setAttribute("errormessages", comments);
			session.setAttribute("getmessage_id", getmessage_id);
			session.setAttribute("errorcomment", comment);
			response.sendRedirect("./");
		}

	}
	private boolean isValid(HttpServletRequest request, List<String> comments){

		String comment = request.getParameter("comment");
		if(!(StringUtils.isBlank(comment))){
			if(!(1 <= comment.length() && comment.length() <= 500)){
				comments.add("コメントの定義に従っていません");

			}
		}else{
			comments.add("コメントの定義に従っていません");
		}

		if(comments.size() == 0){
			return true;
		}else{
			return false;
		}

	}


}
