package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class UserManagementDao {

	public List<Management> getManagement(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.login_id, ");
			sql.append("users.password, ");
			sql.append("users.name, ");
			sql.append("users.branch_id, ");
			sql.append("users.position_id, ");
			sql.append("users.created_date, ");
			sql.append("users.isstoped, ");
			sql.append("branchs.id as branchs_id, ");
			sql.append("branchs.branch_name, ");
			sql.append("positions.id as positions_id, ");
			sql.append("positions.position_name ");
			sql.append("FROM users ");
			sql.append("LEFT JOIN branchs ");
			sql.append("ON users.branch_id = branchs.id ");
			sql.append("LEFT JOIN positions ");
			sql.append("ON users.position_id = positions.id");
			sql.append(" ORDER BY users.branch_id ASC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Management> ret = toManagementList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Management> toManagementList(ResultSet rs)
			throws SQLException {

		List<Management> ret = new ArrayList<Management>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");
				int isstoped = rs.getInt("isstoped");
				int id = rs.getInt("ID");

				Management managements = new Management();
				managements.setLogin_id(login_id);
				managements.setName(name);
				managements.setBranch_name(branch_name);
				managements.setPosition_name(position_name);
				managements.setIsstoped(isstoped);
				managements.setId(id);

				ret.add(managements);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}
