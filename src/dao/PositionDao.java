package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPosition(Connection connection, int num){

		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM positions ";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs)throws SQLException{

		List<Position> ret = new ArrayList<Position>();

		try{
			while(rs.next()){
				String position_name = rs.getString("position_name");
				int id = rs.getInt("id");

				Position positions = new Position();
				positions.setPosition_name(position_name);
				positions.setId(id);
				ret.add(positions);
			}
			return ret;
		}finally{
			close(rs);
		}

	}


}
