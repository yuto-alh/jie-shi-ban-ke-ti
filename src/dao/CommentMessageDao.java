package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentMessageDao {

	public void insert(Connection connection, Comment comment){

		PreparedStatement ps =null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments(");
			sql.append(" user_id");
			sql.append(", comment");
			sql.append(", message_id");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUser_id());
			ps.setString(2, comment.getComment());
			ps.setInt(3, comment.getMessage_id());

			ps.executeUpdate();
		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

}
