package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> getBranch(Connection connection, int num){

		PreparedStatement ps = null;

		try{
			String sql = "SELECT * FROM branchs ";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs)throws SQLException{

		List<Branch> ret = new ArrayList<Branch>();

		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String branch_name = rs.getString("branch_name");

				Branch branchs = new Branch();
				branchs.setId(id);
				branchs.setBranch_name(branch_name);

				ret.add(branchs);
			}
			return ret;
		}finally{
			close(rs);
		}

	}

}
