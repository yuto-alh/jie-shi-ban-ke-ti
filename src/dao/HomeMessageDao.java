package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.HomeMessage;
import exception.SQLRuntimeException;

public class HomeMessageDao {

	public List<HomeMessage> getHomeMessage(Connection connection,
			int num, String getCategory, String start, String stop){

		PreparedStatement ps = null;

		try{


			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id, ");
			sql.append("messages.subject, ");
			sql.append("messages.category, ");
			sql.append("messages.text, ");
			sql.append("messages.user_id, ");
			sql.append("messages.created_date, ");
			sql.append("users.name, ");
			sql.append("users.id as users_id ");
			sql.append("FROM messages ");
			sql.append("LEFT JOIN users ");
			sql.append("ON messages.user_id = users.id");
			sql.append(" where messages.created_date");
			if(!(StringUtils.isEmpty(getCategory))){
				sql.append(" BETWEEN ? AND ?");
				sql.append(" AND category LIKE ?");
				sql.append(" ORDER BY created_date DESC limit " + num);
			}else{
				sql.append(" BETWEEN ? AND ?");
				sql.append(" ORDER BY created_date DESC limit " + num);
			}

			if(StringUtils.isEmpty(start) && StringUtils.isEmpty(stop)){
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, "2000/01/01");
				ps.setString(2, "2100/12/31");



			}else if(StringUtils.isEmpty(start) && !(StringUtils.isEmpty(stop))){
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, "2000/01/01");
				ps.setString(2, stop + " 23:59:59");


			}else if(!(StringUtils.isEmpty(start)) && StringUtils.isEmpty(stop)){
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, start + " 00:00:01");
				ps.setString(2, "2100/12/31");


			}else{
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, start + " 00:00:01");
				ps.setString(2, stop + " 23:59:59");


			}

			if(!(StringUtils.isEmpty(getCategory))){
				ps.setString(3,"%" +  getCategory + "%");

			}

			ResultSet rs = ps.executeQuery();
			List<HomeMessage> ret = toHomeMessageList(rs);
			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<HomeMessage> toHomeMessageList(ResultSet rs) throws SQLException{

		List<HomeMessage> ret = new ArrayList<HomeMessage>();

		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				String text = rs.getString("text");
				String name = rs.getString("name");
				String created_date = rs.getString("created_date");
				created_date = created_date.substring(0, created_date.length() - 2);
				int user_id = rs.getInt("user_id");

				HomeMessage homeMessage = new HomeMessage();
				homeMessage.setId(id);
				homeMessage.setSubject(subject);
				homeMessage.setCategory(category);
				homeMessage.setText(text);
				homeMessage.setName(name);
				homeMessage.setCreated_date(created_date);
				homeMessage.setUser_id(user_id);

				ret.add(homeMessage);

			}
			return ret;
		}finally{
			close(rs);
		}

	}

}
