package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public List<Comment> getComment(Connection connetion, int num){
		PreparedStatement ps = null;
		try{

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id, ");
			sql.append("comments.comment, ");
			sql.append("comments.message_id, ");
			sql.append("comments.user_id, ");
			sql.append("comments.created_date, ");
			sql.append("messages.id as messages_id, ");
			sql.append("users.name, ");
			sql.append("users.id as users_id ");
			sql.append("FROM comments ");
			sql.append("LEFT JOIN messages ");
			sql.append("ON comments.message_id = messages.id ");
			sql.append("LEFT JOIN users ON ");
			sql.append("comments.user_id = users.id");

			ps = connetion.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs) throws SQLException{

		List<Comment> ret = new ArrayList<Comment>();

		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String comment = rs.getString("comment");
				String created_date = rs.getString("created_date");
				created_date = created_date.substring(0, created_date.length() - 2);
				String name = rs.getString("name");
				int user_id = rs.getInt("user_id");
				int message_id = rs.getInt("message_id");
				int messages_id = rs.getInt("messages_id");
				int users_id = rs.getInt("users_id");

				Comment commentList = new Comment();
				commentList.setId(id);
				commentList.setComment(comment);
				commentList.setCreated_date(created_date);
				commentList.setName(name);
				commentList.setUser_id(user_id);
				commentList.setMessage_id(message_id);
				commentList.setMessages_id(messages_id);
				commentList.setUsers_id(users_id);

				ret.add(commentList);

			}
			return ret;

		}finally{
			close(rs);
		}

	}

}
