package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Edit;
import exception.SQLRuntimeException;

public class EditUserDao {

	public Edit getEdit(Connection connection, int editid) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, editid);

			ResultSet rs = ps.executeQuery();

			Edit UserId = userId(rs);


			return UserId;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private Edit userId (ResultSet rs) throws SQLException{


		try{
			if(rs.next()){

				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");

				Edit edit = new Edit();
				edit.setId(id);
				edit.setLogin_id(login_id);
				edit.setPassword(password);
				edit.setName(name);
				edit.setBranch_id(branch_id);
				edit.setPosition_id(position_id);

				return edit ;
			}
				return null;
		}finally{
			close(rs);
		}


	}
}
