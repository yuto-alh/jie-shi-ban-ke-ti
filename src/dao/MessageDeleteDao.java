package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class MessageDeleteDao {

	public void Delete(Connection connection, int messageid){

		PreparedStatement ps = null;

		try{
			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1,  messageid);

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
