package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Edit;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class EditDao {

	public void update(Connection connection, Edit edit){

		PreparedStatement ps = null;

		try{

			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, edit.getLogin_id());
			ps.setString(2, edit.getPassword());
			ps.setString(3, edit.getName());
			ps.setInt(4, edit.getBranch_id());
			ps.setInt(5, edit.getPosition_id());
			ps.setInt(6, edit.getId());

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}
