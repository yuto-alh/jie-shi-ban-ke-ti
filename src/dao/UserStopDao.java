package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserStopDao {

	public void Update(Connection connection, int stop, int userid){

		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" isstoped = ?" );
			sql.append(" where id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,  stop);
			ps.setInt(2, userid);

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

}
