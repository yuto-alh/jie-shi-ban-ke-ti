package dao;

import static Utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class IdCheckDao {

	public User IdCheck(Connection connection, int id){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> idCheckList = IdCheckList(rs);
			if (idCheckList.isEmpty() == true) {
                return null;
            } else if (2 <= idCheckList.size()) {
                throw new IllegalStateException("2 <= idCheckList.size()");
            } else {
                return idCheckList.get(0);
            }

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<User> IdCheckList(ResultSet rs)throws SQLException{
		List<User> ret = new ArrayList<User>();
		try{

			while(rs.next()){

				int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_id = rs.getString("branch_id");
                String position_id = rs.getString("position_id");
                String isstoped = rs.getString("isstoped");
                String createdDate = rs.getString("created_date");
                String updatedDate = rs.getString("updated_date");



				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setPosition_id(position_id);
				user.setIsstoped(isstoped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);


				ret.add(user);
			}
			return ret;
		}finally {
			close(rs);
		}
	}
}
