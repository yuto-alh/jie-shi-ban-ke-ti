package beans;

import java.io.Serializable;

public class Management implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String password;
    private String name;
    private int branch_id;
    private int position_id;
    private int isstoped;
    private String createdDate;
    private int branchs_id;
    private String branch_name;
    private int positions_id;
    private String position_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getPosition_id() {
		return position_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}

	public int getBranchs_id() {
		return branchs_id;
	}
	public void setBranchs_id(int branchs_id) {
		this.branchs_id = branchs_id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public int getPositions_id() {
		return positions_id;
	}
	public void setPositions_id(int positions_id) {
		this.positions_id = positions_id;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public int getIsstoped() {
		return isstoped;
	}
	public void setIsstoped(int isstoped) {
		this.isstoped = isstoped;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

}
