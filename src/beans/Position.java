package beans;

import java.io.Serializable;

public class Position implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String position_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}


}
