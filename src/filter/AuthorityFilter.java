package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {("/management"), ("/edit"), ("/signup") })
public class AuthorityFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");



		if(user !=null){
			int position = Integer.parseInt(user.getPosition_id());
			int branch = Integer.parseInt(user.getBranch_id());

			if(position == 1 && branch == 1){

				chain.doFilter(request, response);
			}else{
				String filtermessage = "・権限がありません";
				session.setAttribute("filtermessage", filtermessage);

				((HttpServletResponse)response).sendRedirect("./");
			}
		}else{
			chain.doFilter(request, response);
		}

	}
	public void init(FilterConfig config){

	}
	public void destroy() {

	}

}
