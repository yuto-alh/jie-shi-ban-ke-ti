package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {





	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		String url = ((HttpServletRequest)request).getServletPath();



		if(!(url.contains("login"))){

			if(session == null){
				String Loginmessage = "・ログインしてください";
				System.out.println(Loginmessage);
				session.setAttribute("Loginmessage", Loginmessage);
				((HttpServletResponse)response).sendRedirect("login");
			}else if(session.getAttribute("loginUser") == null){
				String Loginmessage = "・ログインしてください";
				session.setAttribute("Loginmessage", Loginmessage);
				((HttpServletResponse)response).sendRedirect("login");


			}else{
				chain.doFilter(request, response);
			}

		}else{
			chain.doFilter(request, response);
		}

	}
	public void init(FilterConfig config){

	}
	public void destroy() {

	}

}
