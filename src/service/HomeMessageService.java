package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.HomeMessage;
import dao.HomeMessageDao;

public class HomeMessageService {

	private static final int LIMIT_NUM = 1000;

	public List<HomeMessage> getHomeMessage(String getCategory, String start, String stop){

		Connection connection = null;
		try{
			connection = getConnection();

			HomeMessageDao homeMessageDao = new HomeMessageDao();
			List<HomeMessage> ret = homeMessageDao.getHomeMessage(connection, LIMIT_NUM, getCategory,start,stop);

			commit(connection);
			return ret;

		}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
