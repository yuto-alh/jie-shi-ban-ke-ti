package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import beans.Comment;
import dao.CommentMessageDao;

public class CommentMessageService {

	public void Commentmessage(Comment comment) {

		Connection connection = null;

		try{
			connection = getConnection();

			CommentMessageDao commentMessageDao = new CommentMessageDao();
			commentMessageDao.insert(connection, comment);

			commit(connection);
		}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
