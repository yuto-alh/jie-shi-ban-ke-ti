package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService {
	private static final int LIMIT_NUM = 1000;

	public List<Branch> getBranch(){

		Connection conneciton = null;
		try{
			conneciton = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranch(conneciton, LIMIT_NUM);

			commit(conneciton);

			return ret;
		}catch(RuntimeException e){
			rollback(conneciton);
			throw e;
		}catch (Error e) {
			rollback(conneciton);
			throw e;
		}finally {
			close(conneciton);
		}
	}

}
