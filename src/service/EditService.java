package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import beans.Edit;
import dao.EditUserDao;

public class EditService {

	public Edit getEdit(int editId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        EditUserDao editUserDao = new EditUserDao();
	        Edit edit = editUserDao.getEdit(connection, editId);

	        commit(connection);

	        return edit;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
