package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import dao.LoginCheckDao;

public class LoginIdCheck {

	public static boolean Logincheck(String login_id){

		Connection connection = null;
		try{
			connection = getConnection();

			LoginCheckDao checkDao = new LoginCheckDao();
			boolean user = checkDao.CheckId(connection,login_id);

			commit(connection);
			return user;
		}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
