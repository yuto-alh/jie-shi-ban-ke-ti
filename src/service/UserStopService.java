package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import dao.UserStopDao;

public class UserStopService {

	public void getStop(int stop, int userId){

		Connection connection = null;
		try{
			connection = getConnection();

			UserStopDao stopDao = new UserStopDao();
			stopDao.Update(connection, stop, userId);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

}
