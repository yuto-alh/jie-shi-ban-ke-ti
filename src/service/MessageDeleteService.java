package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import dao.MessageDeleteDao;

public class MessageDeleteService {


	public void getMessage(int messageId){

		Connection connection = null;

		try{
			connection = getConnection();

			MessageDeleteDao messageDeleteDao = new MessageDeleteDao();
			messageDeleteDao.Delete(connection, messageId);

			commit(connection);
		} catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

}
