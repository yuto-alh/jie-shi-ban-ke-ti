package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.IdCheckDao;

public class IdCheckService {

	public static User IdCheck(int id){

		Connection connection = null;


		try{
			connection = getConnection();
			IdCheckDao idCheckDao = new IdCheckDao();
			User user = idCheckDao.IdCheck(connection, id);

			commit(connection);
			return user;
		}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
