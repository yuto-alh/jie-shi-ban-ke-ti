package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;

public class CommentService {
	private static final int LIMIT_NUM = 1000;

	public List<Comment> getComment(){

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			List<Comment> ret = commentDao.getComment(connection, LIMIT_NUM);

			commit(connection);
			return ret;

		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

	}

}
