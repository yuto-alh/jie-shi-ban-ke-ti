package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import Utils.CipherUtil;
import beans.Edit;
import dao.EditDao;

public class EditCustomService {

	public void update(Edit edit, String getPassword, String inputPassword){

		Connection connection = null;
		try{
			connection = getConnection();

			if(!(StringUtils.isEmpty(inputPassword) == true)){
				String encPassword = CipherUtil.encrypt(edit.getPassword());
				edit.setPassword(encPassword);

			}


			EditDao editDao = new EditDao();
			editDao.update(connection, edit);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}
}
