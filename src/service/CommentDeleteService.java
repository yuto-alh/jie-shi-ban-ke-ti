package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;

import dao.CommentDeleteDao;

public class CommentDeleteService {

	public void getComments(int commentId){

		Connection connection = null;

		try{
			connection = getConnection();

			CommentDeleteDao commentDeleteDao = new CommentDeleteDao();
			commentDeleteDao.Delete(connection, commentId);

			commit(connection);
		} catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

}
