package service;

import static Utils.CloseableUtil.*;
import static Utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Management;
import dao.UserManagementDao;

public class ManagementService {
    private static final int LIMIT_NUM = 1000;

    public List<Management> getManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserManagementDao managementDao = new UserManagementDao();
            List<Management> ret = managementDao.getManagement(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
